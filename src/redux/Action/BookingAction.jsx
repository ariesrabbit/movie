import { ADD_BOOKING_LIST_CHAIR } from "../constant/BookingConstant";

export let handleBookingChair = (chair) => {
  return {
    type: ADD_BOOKING_LIST_CHAIR,
    payload: chair,
  };
};
