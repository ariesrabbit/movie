import React from "react";
import HeaderPage from "../../components/Header/HeaderPage";
import BannerHomePage from "./Banner/BannerHomePage";
import ListMovie from "./MovieList/ListMovie";
import MovieTabs from "./MovieTab/MovieTabs";

export default function HomePage() {
  return (
    <div>
      <HeaderPage />
      <BannerHomePage />
      <ListMovie />
      <MovieTabs />
    </div>
  );
}
